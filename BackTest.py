from RandomMakerTree import RandomMakerTree


def get_data():
	global data
	data = []
	item = {}
	f = open('history.txt')
	for line in iter(f):
		item = eval(line)
		data.append(item)
	f.close()
	return data 


def getAdvice(node,item):
	if (node.type == 'NonTerminal'):
		if (node.Equal(item)):
			print " "+str(node)+"(TRUE)-->",
			getAdvice(node.right,item)
		else:
			print " "+str(node)+"(FALSE)-->",
			getAdvice(node.left,item)
	if (node.type == 'Terminal'):
		print node


if __name__ == '__main__':
	
	rmt = RandomMakerTree('test.csv')
	rmt.setNames()
	root = rmt.MakeTree()
	data = get_data()

	for item in data:
		print item
		getAdvice(root, item)

