 # -*- coding: utf-8 -*-
class RandomMakerTree(object):
	"""RandomMakerTree
	служит для генерации дерева,на входе только csv файл 
	нужен чтобы знать кол-во индикаторов и их имена"""
	


	def __init__(self, csv_path):
		self.csv_path = csv_path

	def setNames(self):
		
		import csv
		headers = ""
		
		with open (self.csv_path,'rb') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
			headers = spamreader.next()[0] #читаем первую строку файла

		headers = headers.split(',') 
	
		headers = [x for x in headers if x!='time' and x!= 'buy' and x!='sell']  #убираем ненужные значения 
		self.headers = headers
		self.count = len(headers)


	def RandomNonTerminal(self):
		#создаёт случайный нетерминальный узел
		from Node import NonTerminalNode
		import random
		from params import VALUES
		return NonTerminalNode(random.choice(self.headers),random.choice(VALUES))


	def RandomTerminal(self):
		from Node import TerminalNode
		
		import random
		return TerminalNode(random.choice(['BUY','SELL']))


	def RandomNode(self):
		import random

		if (random.choice([True,False])):
			return self.RandomNonTerminal()
		else:
			return self.RandomTerminal()


	def MakeTreeRecursive(self,current,level):
		if (current.type == 'NonTerminal'):
			if (level==self.count):
				current.left = self.RandomTerminal()
				current.right = current.left.mutate()
				return

			left = self.RandomNode()
			right = self.RandomNode()
			
			if (str(left) == str(right)):
				right = left.mutate()

			current.left = left
			self.MakeTreeRecursive(current.left,level+1)
			
			current.right = right
			self.MakeTreeRecursive(current.right,level+1)
		
	def MakeTree(self):
		root = self.RandomNonTerminal()
		self.MakeTreeRecursive(root,0)
		return root



"""Генерация случайных деревьев"""



def printTree(node,level):
	if (node.type == 'NonTerminal'):
		printTree(node.left,level+1)
		print "| " * level+ str(node)
		level-=1
		printTree(node.right, level+1)

if __name__ == '__main__':
	
	rmt = RandomMakerTree('test.csv')
	rmt.setNames()
	Tree = rmt.MakeTree()
	printTree(Tree,1)