 # -*- coding: utf-8 -*-
from abc import ABCMeta

class Node:
	"""Абстрактный класс узла"""
	@property
	def type(self):
	    raise NotImplementedError


class TerminalNode(Node):
	"""Листовые объекты, могут принимать два значения
		buy, sell"""

	type = "Terminal"

	def __init__(self,signal):
		self.signal = signal
		#В конструкторе принимаем значение BUY, SELL

	#операция мутации (реверс сигнала)
	def mutate(self):
		return TerminalNode('BUY') if self.signal == 'SELL' else TerminalNode('SELL')

	def __str__(self):
		return str(self.signal)

class NonTerminalNode(Node):
	"""Нетерминальная вершина, содержит в себе предикат,
	левого и правого потомка.
	условимся что переход к правому потомку если предикат возвращает истину,
	в противном случае переход к левому потомку """

	type = "NonTerminal"
	
	def __init__(self, index, rate):
		
		self.index = index #имя индикатора
		self.rate = rate #значение индикатора
		
	"""Присваиваем потомков"""

	def setLeft(self,left):
		self.left = left

		
	def setRight(self,right):
		self.right = right


	def Equal(self,item):
		return item[self.index] == self.rate


	def mutate(self):
		from params import VALUES
		return NonTerminalNode(self.index, (VALUES.index(self.rate)+1)%6)


	def __str__(self):
		return "if " + str(self.index) + " is " + str(self.rate) 